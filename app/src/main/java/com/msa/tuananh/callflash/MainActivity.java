package com.msa.tuananh.callflash;

import android.Manifest;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.msa.tuananh.callflash.noti.ActivityNotification;
import com.msa.tuananh.callflash.ui.dialog.SettingLightDialog;
import com.msa.tuananh.callflash.utils.Constant;
import com.msa.tuananh.callflash.utils.FlashUtilities;
import com.msa.tuananh.callflash.utils.Utilities;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    SwitchCompat flashable, cummingCall, cummingSms, disturb, soundNormal, soundVibrate, soundSilent, sw_noti;
    LinearLayout batteryItem, startDisturb, endDisturb, noti, shareApps, rateUs, call, sms;
    TextView batteryLvl, tvDisturbStart, tvDisturbEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        setDefault();
    }

    private void initView() {

        flashable = findViewById(R.id.sw_flashable);
        cummingCall = findViewById(R.id.sw_cummingCALL);
        cummingSms = findViewById(R.id.sw_cummingSMS);
        disturb = findViewById(R.id.sw_disturbMode);
        soundNormal = findViewById(R.id.sw_soundNormal);
        soundVibrate = findViewById(R.id.sw_soundVibrate);
        soundSilent = findViewById(R.id.sw_soundSilent);
        batteryItem = findViewById(R.id.item_battery);
        batteryLvl = findViewById(R.id.tv_batLvl);
        startDisturb = findViewById(R.id.item_disturb_start);
        endDisturb = findViewById(R.id.item_disturb_end);
        tvDisturbStart = findViewById(R.id.tv_disturb_start);
        tvDisturbEnd = findViewById(R.id.tv_disturb_end);
        sw_noti = findViewById(R.id.sw_noti);
        noti = findViewById(R.id.item_noti);

        shareApps = findViewById(R.id.item_share);
        rateUs = findViewById(R.id.item_rate);

        call = findViewById(R.id.item_status_call);
        sms = findViewById(R.id.item_status_sms);

        disturb.setOnCheckedChangeListener(this);
        flashable.setOnCheckedChangeListener(this);
        cummingCall.setOnCheckedChangeListener(this);
        cummingSms.setOnCheckedChangeListener(this);
        soundNormal.setOnCheckedChangeListener(this);
        soundVibrate.setOnCheckedChangeListener(this);
        soundSilent.setOnCheckedChangeListener(this);
        startDisturb.setOnClickListener(this);
        endDisturb.setOnClickListener(this);
        batteryItem.setOnClickListener(this);
        noti.setOnClickListener(this);
        sw_noti.setOnCheckedChangeListener(this);
        shareApps.setOnClickListener(this);
        rateUs.setOnClickListener(this);

        call.setOnClickListener(this);
        sms.setOnClickListener(this);
    }

    private void setDefault() {
        flashable.setChecked(Utilities.getSettings(this, "Service"));
        cummingCall.setChecked(Utilities.getSettings(this, "Call"));
        cummingSms.setChecked(Utilities.getSettings(this, "Sms"));

        disturb.setChecked(Utilities.getDisturb(this));

        soundNormal.setChecked(Utilities.getSoundProfile(this, "Normal"));
        soundVibrate.setChecked(Utilities.getSoundProfile(this, "Vibrate"));
        soundSilent.setChecked(Utilities.getSoundProfile(this, "Silent"));

        batteryLvl.setText(String.valueOf(Utilities.getBatteryLevel(this)) + " %");
        tvDisturbStart.setText(Utilities.getDisturbStart(this));
        tvDisturbEnd.setText(Utilities.getDisturbEnd(this));

        if (FlashUtilities.isNotificationAccessGiven(this)) {
            sw_noti.setChecked(Utilities.getNoti(this));
        } else {
            Utilities.saveNoti(this, false);
            sw_noti.setChecked(false);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {
            case R.id.sw_flashable:
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    flashable.setChecked(false);
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.CAMERA},
                            Constant.FALSH_REQUEST);
                } else {
                    Utilities.saveSettings(MainActivity.this, 1, isChecked, isChecked, isChecked);
                }
                break;
            case R.id.sw_cummingCALL:

                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_PHONE_STATE)
                        != PackageManager.PERMISSION_GRANTED) {
                    cummingCall.setChecked(false);
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_PHONE_STATE},
                            Constant.CALL_REQUEST);
                } else {
                    Utilities.saveSettings(MainActivity.this, 2, isChecked, isChecked, isChecked);
                }

                break;
            case R.id.sw_cummingSMS:

                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.RECEIVE_SMS)
                        != PackageManager.PERMISSION_GRANTED) {
                    cummingSms.setChecked(false);
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.RECEIVE_SMS},
                            Constant.SMS_REQUEST);
                } else {
                    Utilities.saveSettings(MainActivity.this, 3, isChecked, isChecked, isChecked);
                }

                break;
            case R.id.sw_disturbMode:
                Utilities.saveDisturbMode(MainActivity.this, 1, isChecked, "00:00", "00:00");
                break;
            case R.id.sw_soundNormal:
                Utilities.saveSoundProfile(MainActivity.this, 1, isChecked, isChecked, isChecked);
                break;
            case R.id.sw_soundVibrate:
                Utilities.saveSoundProfile(MainActivity.this, 2, isChecked, isChecked, isChecked);
                break;
            case R.id.sw_soundSilent:
                Utilities.saveSoundProfile(MainActivity.this, 3, isChecked, isChecked, isChecked);
                break;
            case R.id.sw_noti:
                if (FlashUtilities.isNotificationAccessGiven(this)) {
                    Utilities.saveNoti(MainActivity.this, true);
                } else {
                    sw_noti.setChecked(false);
                    startActivityForResult(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"), Constant.NOTIFI_REQUEST);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.item_battery:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(null);
                View dialogView = LayoutInflater.from(this).inflate(R.layout.alert_battery, null);
                builder.setView(dialogView);
                final SeekBar seekBar = dialogView.findViewById(R.id.alert_bat_seekbar);
                seekBar.setProgress(Utilities.getBatteryLevel(MainActivity.this));

                final TextView title = dialogView.findViewById(R.id.alert_bat_tvTitle);
                title.setText("Battery Level Threshold: " + Utilities.getBatteryLevel(getApplicationContext()) + " %");
                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        title.setText("Battery Level Threshold: " + String.valueOf(progress) + " %");
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

                final Button cancel = dialogView.findViewById(R.id.alert_cancel);
                final Button save = dialogView.findViewById(R.id.alert_save);

                final AlertDialog alertDialog = builder.create();

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        batteryLvl.setText(String.valueOf(seekBar.getProgress()) + " %");
                        Utilities.saveBatteryLevel(MainActivity.this, seekBar.getProgress());
                        alertDialog.dismiss();
                    }
                });

                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertDialog.show();
                break;

            case R.id.item_disturb_start:
                showHourPicker(2);
                break;

            case R.id.item_disturb_end:
                showHourPicker(3);
                break;

            case R.id.item_noti:
                Intent intent = new Intent(MainActivity.this, ActivityNotification.class);
                startActivity(intent);
                break;

            case R.id.item_share:
                FlashUtilities.shareApp(this);
                break;

            case R.id.item_rate:
                FlashUtilities.rateUs(this);
                break;

            case R.id.item_status_call:
                showlightDialog("Incomming Call", 1);
                break;

            case R.id.item_status_sms:
                showlightDialog("Incomming Sms", 2);
                break;
        }
    }

    public void showHourPicker(final int type) {
        final Calendar myCalender = Calendar.getInstance();
        final int hour = myCalender.get(Calendar.HOUR_OF_DAY);
        int minute = myCalender.get(Calendar.MINUTE);

        TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (view.isShown()) {
                    String time = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                    Utilities.saveDisturbMode(MainActivity.this, type, false, time, time);
                    Toast.makeText(MainActivity.this, time, Toast.LENGTH_SHORT).show();
                    if (type == 2) {
                        tvDisturbStart.setText(Utilities.getDisturbStart(MainActivity.this));
                    } else if (type == 3) {
                        tvDisturbEnd.setText(Utilities.getDisturbEnd(MainActivity.this));
                    }
                }
            }
        };

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
        timePickerDialog.setTitle("Choose hour:");
        timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        timePickerDialog.show();
    }

    private void showlightDialog(String title, Integer type) {
        FragmentManager fm = getSupportFragmentManager();
        SettingLightDialog editNameDialogFragment = SettingLightDialog.newInstance(title, type);
        editNameDialogFragment.show(fm, "fragment_edit_name");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constant.NOTIFI_REQUEST:
                if (FlashUtilities.isNotificationAccessGiven(this)) {
                    sw_noti.setChecked(true);
                } else {
                    sw_noti.setChecked(false);
                    Toast.makeText(this, "We have to use 'Notification access' permission to listen 3rd party app's notification!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constant.FALSH_REQUEST:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    flashable.setChecked(true);
                    Utilities.saveSettings(MainActivity.this, 1, true, true, true);
                } else {
                    flashable.setChecked(false);
                    Toast.makeText(this, "We have to use 'Camera' permission to control the flash!", Toast.LENGTH_SHORT).show();
                }
                break;
            case Constant.CALL_REQUEST:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cummingCall.setChecked(true);
                    Utilities.saveSettings(MainActivity.this, 2, true, true, true);
                } else {
                    cummingCall.setChecked(false);
                    Toast.makeText(this, "We have to use 'Read phone state' permission!", Toast.LENGTH_SHORT).show();
                }
                break;
            case Constant.SMS_REQUEST:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cummingSms.setChecked(true);
                    Utilities.saveSettings(MainActivity.this, 3, true, true, true);
                } else {
                    cummingSms.setChecked(false);
                    Toast.makeText(this, "We have to use 'Receive sms' permission!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
