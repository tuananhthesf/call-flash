package com.msa.tuananh.callflash.noti;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.msa.tuananh.callflash.R;
import com.msa.tuananh.callflash.utils.Utilities;

import java.util.List;

public class MyNotiAdapter extends RecyclerView.Adapter<MyNotiAdapter.ViewHolder> {

    Context mContext;
    List<MyNotiInfo> mList;

    public MyNotiAdapter(Context mContext, List<MyNotiInfo> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.custom_noti, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final MyNotiInfo myNotiInfo = mList.get(position);
        holder.imageView.setImageDrawable(myNotiInfo.image);
        holder.app.setText(myNotiInfo.getTitle());

        holder.checkBox.setOnCheckedChangeListener(null);

        holder.checkBox.setChecked(Utilities.getPackage(mContext, myNotiInfo.pack));

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Utilities.savePackage(mContext, myNotiInfo.getPack(), isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView app;
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.custom_noti_img);
            app = itemView.findViewById(R.id.custom_noti_app);
            checkBox = itemView.findViewById(R.id.custom_noti_check);
        }
    }
}