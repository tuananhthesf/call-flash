package com.msa.tuananh.callflash.ui.dialog;


import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.msa.tuananh.callflash.R;
import com.msa.tuananh.callflash.utils.Constant;
import com.msa.tuananh.callflash.utils.FlashlightProvider;
import com.msa.tuananh.callflash.utils.Utilities;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingLightDialog extends DialogFragment {

    TextView title, onLength, offlength, times;
    SeekBar skOn, skOff, skTime;
    LinearLayout lnTimes;
    Button btn;

    int type;

    Runnable runnable;
    Runnable runnable1;
    Integer timesNhapNhay = 0;
    Integer timesThreshold = 3;
    FlashlightProvider flashlightProvider;
    Handler handler;

    public SettingLightDialog() {
        // Required empty public constructor
    }

    public static SettingLightDialog newInstance(String title, int type) {
        SettingLightDialog frag = new SettingLightDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putInt("type", type);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.dialog_light, container, false);
        getDialog().getWindow().setBackgroundDrawable(new android.graphics.drawable.ColorDrawable(android.graphics.Color.TRANSPARENT));
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        type = getArguments().getInt("type");

        initView(view);


        if (type == 1) {
            lnTimes.setVisibility(View.GONE);
        } else {
            lnTimes.setVisibility(View.VISIBLE);
            initViewSms(view);
        }

        initDefault(type);
        skChange(type);

        flashlightProvider = new FlashlightProvider(getContext());
        handler = new Handler();
        runnable1 = new Runnable() {
            @Override
            public void run() {
                timesNhapNhay += 1;
                Log.d("flash", "off: ");
                flashlightProvider.turnFlashlightOff();
                if (type == 2) {
                    timesThreshold = Utilities.getSmsLight(getContext(), Constant.SMS_TIME);
                    if (timesNhapNhay == timesThreshold) {
                        flashlightProvider.turnFlashlightOff();
                        handler.removeCallbacks(this);
                        handler.removeCallbacks(runnable);
                        btn.setText("Test");
                    } else {
                        handler.postDelayed(runnable, Utilities.getSmsLight(getContext(), Constant.SMS_OFF));
                    }
                } else {
                    handler.postDelayed(runnable, Utilities.getCallLight(getContext(), Constant.CALL_OFF));
                }
            }
        };
        runnable = new Runnable() {
            @Override
            public void run() {
                flashlightProvider.turnFlashlightOn();
                btn.setText("Stop");
                Log.d("flash", "on: ");
                if (type == 1) {
                    handler.postDelayed(runnable1, Utilities.getCallLight(getContext(), Constant.CALL_ON));
                } else {
                    handler.postDelayed(runnable1, Utilities.getCallLight(getContext(), Constant.SMS_ON));
                }
            }
        };

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn.getText().toString().equalsIgnoreCase("Stop")) {
                    flashlightProvider.turnFlashlightOff();
                    handler.removeCallbacks(runnable1);
                    handler.removeCallbacks(runnable);
                    btn.setText("Test");
                } else {
                    if (type == 1) {
                        handler.postDelayed(runnable, Utilities.getCallLight(getContext(), Constant.CALL_OFF));
                    } else {
                        handler.postDelayed(runnable, Utilities.getSmsLight(getContext(), Constant.SMS_OFF));
                    }
                }
            }
        });
    }

    private void initView(View rootView) {
        title = rootView.findViewById(R.id.dialog_title);
        onLength = rootView.findViewById(R.id.dialog_onLength);
        offlength = rootView.findViewById(R.id.dialog_offLength);

        skOn = rootView.findViewById(R.id.dialog_sk_on);
        skOff = rootView.findViewById(R.id.dialog_sk_off);

        lnTimes = rootView.findViewById(R.id.dialog_lnTimes);

        title.setText(getArguments().getString("title"));

        btn = rootView.findViewById(R.id.dialog_btn);
    }

    void initViewSms(View rootView) {
        times = rootView.findViewById(R.id.dialog_times);
        skTime = rootView.findViewById(R.id.dialog_sk_times);
    }

    void initDefault(int type) {
        if (type == 1) {
            onLength.setText(Utilities.getCallLight(getContext(), Constant.CALL_ON) + " ms");
            offlength.setText(Utilities.getCallLight(getContext(), Constant.CALL_OFF) + " ms");

            skOn.setProgress(Utilities.getCallLight(getContext(), Constant.CALL_ON) / 500);
            skOff.setProgress(Utilities.getCallLight(getContext(), Constant.CALL_OFF) / 500);
        } else {
            onLength.setText(Utilities.getSmsLight(getContext(), Constant.SMS_ON) + " ms");
            offlength.setText(Utilities.getSmsLight(getContext(), Constant.SMS_OFF) + " ms");

            skOn.setProgress(Utilities.getSmsLight(getContext(), Constant.SMS_ON) / 500);
            skOff.setProgress(Utilities.getSmsLight(getContext(), Constant.SMS_OFF) / 500);
            skTime.setProgress(Utilities.getSmsLight(getContext(), Constant.SMS_TIME) - 1);

            times.setText(Utilities.getSmsLight(getContext(), Constant.SMS_TIME) + " time(s)");
        }
    }

    void skChange(final int type) {
        skOn.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    if (type == 1) {
                        Utilities.saveCallLight(getContext(), Constant.CALL_ON, 500);
                        onLength.setText(Utilities.getCallLight(getContext(), Constant.CALL_ON) + " ms");
                    } else {
                        Utilities.saveSmsLight(getContext(), Constant.SMS_ON, 500);
                        onLength.setText(Utilities.getSmsLight(getContext(), Constant.SMS_ON) + " ms");
                    }
                } else {
                    if (type == 1) {
                        Utilities.saveCallLight(getContext(), Constant.CALL_ON, progress * 500);
                        onLength.setText(Utilities.getCallLight(getContext(), Constant.CALL_ON) + " ms");
                    } else {
                        Utilities.saveSmsLight(getContext(), Constant.SMS_ON, progress * 500);
                        onLength.setText(Utilities.getSmsLight(getContext(), Constant.SMS_ON) + " ms");
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        skOff.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress == 0) {
                    if (type == 1) {
                        Utilities.saveCallLight(getContext(), Constant.CALL_OFF, 500);
                        offlength.setText(Utilities.getCallLight(getContext(), Constant.CALL_OFF) + " ms");
                    } else {
                        Utilities.saveSmsLight(getContext(), Constant.SMS_OFF, 500);
                        offlength.setText(Utilities.getSmsLight(getContext(), Constant.SMS_OFF) + " ms");
                    }
                } else {
                    if (type == 1) {
                        Utilities.saveCallLight(getContext(), Constant.CALL_OFF, progress * 500);
                        offlength.setText(Utilities.getCallLight(getContext(), Constant.CALL_OFF) + " ms");
                    } else {
                        Utilities.saveSmsLight(getContext(), Constant.SMS_OFF, progress * 500);
                        offlength.setText(Utilities.getSmsLight(getContext(), Constant.SMS_OFF) + " ms");
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        if (type == 2) {
            skTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    Utilities.saveSmsLight(getContext(), Constant.SMS_TIME, progress + 1);
                    times.setText(Utilities.getSmsLight(getContext(), Constant.SMS_TIME) + " time(s)");
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        flashlightProvider.turnFlashlightOff();
        handler.removeCallbacks(runnable1);
        handler.removeCallbacks(runnable);
        super.onDismiss(dialog);
    }
}
