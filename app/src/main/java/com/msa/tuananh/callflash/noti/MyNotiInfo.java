package com.msa.tuananh.callflash.noti;

import android.graphics.drawable.Drawable;

public class MyNotiInfo {
    public String pack, title;
    public Drawable image;
    public Boolean isChecked;

    public MyNotiInfo(String pack, String title, Drawable image, Boolean isChecked) {
        this.pack = pack;
        this.title = title;
        this.image = image;
        this.isChecked = isChecked;
    }

    public MyNotiInfo(String pack, String title, Drawable image) {
        this.pack = pack;
        this.title = title;
        this.image = image;
    }

    public MyNotiInfo(String pack, String title) {
        this.pack = pack;
        this.title = title;
    }

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }
}
