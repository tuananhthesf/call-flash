package com.msa.tuananh.callflash.utils;

public class Constant {
    public static final String CALL_ON = "CallOn";
    public static final String CALL_OFF = "CallOff";

    public static final String SMS_ON = "SmsOn";
    public static final String SMS_OFF = "SmsOff";
    public static final String SMS_TIME = "SmsTime";

    public static final int NOTIFI_REQUEST = 1771;

    public static final int FALSH_REQUEST = 7788;
    public static final int CALL_REQUEST = 8899;
    public static final int SMS_REQUEST = 9900;

}
