package com.msa.tuananh.callflash.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.msa.tuananh.callflash.utils.Constant;
import com.msa.tuananh.callflash.utils.FlashlightProvider;
import com.msa.tuananh.callflash.utils.Utilities;


public class NotificationServiceFlash extends NotificationListenerService {
    Context context;
    Runnable runnable;
    Runnable runnable1;
    Integer timesNhapNhay = 0;
    FlashlightProvider flashlightProvider;
    Handler handler;



    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        Log.d("notii", "onNotificationPosted: ");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);
        flashlightProvider = new FlashlightProvider(context);
        handler = new Handler();

        runnable1 = new Runnable() {
            @Override
            public void run() {
                timesNhapNhay += 1;
                Log.d("flash", "off: ");
                flashlightProvider.turnFlashlightOff();
                if (timesNhapNhay == Utilities.getSmsLight(context, Constant.SMS_TIME)) {
                    flashlightProvider.turnFlashlightOff();
                    handler.removeCallbacks(this);
                    handler.removeCallbacks(runnable);
                } else {
                    handler.postDelayed(runnable,  Utilities.getSmsLight(context, Constant.SMS_OFF));
                }
            }
        };
        runnable = new Runnable() {
            @Override
            public void run() {
                flashlightProvider.turnFlashlightOn();
                Log.d("flash", "on: ");
                handler.postDelayed(runnable1,  Utilities.getSmsLight(context, Constant.SMS_ON));
            }
        };

        String pack = sbn.getPackageName();

        Log.d("notii", "onNotificationPosted: " + pack);
        
        if (Utilities.getNoti(context)) {
            if (Utilities.getPackage(context, pack)) {
                handler.postDelayed(runnable,  Utilities.getSmsLight(context, Constant.SMS_OFF));
            }
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
    }
}
