package com.msa.tuananh.callflash.noti;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.msa.tuananh.callflash.R;
import com.msa.tuananh.callflash.utils.FlashUtilities;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ActivityNotification extends AppCompatActivity {

    List<MyNotiInfo> mList;
    MyNotiAdapter adapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initRv();
    }

    private void initRv() {
        recyclerView = findViewById(R.id.rv_noti);
        mList = new ArrayList<>();
        FlashUtilities.myPackageInfo(this, mList);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mList.sort(new Comparator<MyNotiInfo>() {
                @Override
                public int compare(MyNotiInfo o1, MyNotiInfo o2) {
                    return o1.getTitle().compareTo(o2.getTitle());
                }
            });
        }
        adapter = new MyNotiAdapter(this, mList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }
}
