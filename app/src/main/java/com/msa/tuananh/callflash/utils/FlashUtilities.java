package com.msa.tuananh.callflash.utils;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.Toast;

import com.msa.tuananh.callflash.noti.MyNotiInfo;

import java.util.Calendar;
import java.util.List;
import java.util.Set;

public class FlashUtilities {

    public static void switchServiceLife(Context context, Class service, Boolean isLife) {
        PackageManager pm = context.getPackageManager();
        ComponentName componentName = new ComponentName(context, service);
        if (isLife) {
            pm.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);
        } else {
            pm.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT,
                    PackageManager.DONT_KILL_APP);
        }
    }

    public static String getSoundProfile(Context context) {
        String type = "";
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        switch (am.getRingerMode()) {
            case AudioManager.RINGER_MODE_SILENT:
                Log.i("MyApp", "Silent mode");
                type = "Silent";
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                Log.i("MyApp", "Vibrate mode");
                type = "Vibrate";
                break;
            case AudioManager.RINGER_MODE_NORMAL:
                Log.i("MyApp", "Normal mode");
                type = "Normal";
                break;
        }
        return type;
    }

    public static int getBatteryPercentage(Context context) {

        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);

        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

        float batteryPct = level / (float) scale;

        return (int) (batteryPct * 100);
    }

    public static Integer[] getCurrentTime() {
        Integer[] hour = new Integer[2];
        Calendar rightNow = Calendar.getInstance();
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);
        int currentMinute = rightNow.get(Calendar.MINUTE);
        hour[0] = currentHour;
        hour[1] = currentMinute;
        return hour;
    }

    public static Boolean dobHours(Context context) {
        Boolean isDob = false;

        String[] start = Utilities.getDisturbStart(context).split(":");
        String[] end = Utilities.getDisturbEnd(context).split(":");

        int startH = Integer.valueOf(start[0]);
        int startM = Integer.valueOf(start[1]);

        int endH = Integer.valueOf(end[0]);
        int endM = Integer.valueOf(end[0]);

        int currentH = getCurrentTime()[0];
        int currentM = getCurrentTime()[1];

        if (startH < endH) {
            if (currentH >= startH && currentH <= endH) {
                if (currentH == startH) {
                    if (currentM >= startM) {
                        isDob = true;
                    } else {
                        isDob = false;
                    }
                } else if (currentH == endH) {
                    if (currentM <= endM) {
                        isDob = true;
                    } else {
                        isDob = false;
                    }
                } else {
                    isDob = true;
                }
            }
        } else if (startH > endH) {
            if (currentH >= endH && currentH <= startH) {
                if (currentH == endH) {
                    if (currentM >= endM) {
                        isDob = false;
                    } else {
                        isDob = true;
                    }
                } else if (currentH == startH) {
                    if (currentM <= startM) {
                        isDob = false;
                    } else {
                        isDob = true;
                    }
                } else {
                    isDob = false;
                }
            } else {
                isDob = true;
            }
        } else if (startH == endH) {
            if (startM <= endM) {
                if (currentH == startH) {
                    if (currentM >= startM && currentM <= endM) {
                        isDob = true;
                    } else {
                        isDob = false;
                    }
                } else {
                    isDob = false;
                }
            } else {
                if (currentH == startH) {
                    for (int i = endM; i <= startM; i++) {
                        if (currentM == i) {
                            isDob = false;
                        } else {
                            isDob = true;
                        }
                    }
                } else {
                    isDob = true;
                }
            }
        }
        return isDob;
    }

    public static void myPackageInfo(Context context, List<MyNotiInfo> mList) {
        final PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        for (ApplicationInfo packageInfo : packages) {
            MyNotiInfo myNotiInfo = null;
            try {
                myNotiInfo = new MyNotiInfo(packageInfo.packageName, pm.getApplicationLabel(packageInfo).toString(), pm.getApplicationIcon(packageInfo.packageName));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            mList.add(myNotiInfo);
            Log.d("package", "getPackageInfo: " + mList.get(0).getTitle());
        }
    }

    public static void rateUs(Activity activity) {
        Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            activity.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(activity, "Couln't launch the market", Toast.LENGTH_SHORT).show();
        }
    }

    public static void shareApp(Activity activity) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "tuan anh dep trai.";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Do something");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        activity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public static boolean isAccessibilityEnabled(Context context, String id) {

        AccessibilityManager am = (AccessibilityManager) context
                .getSystemService(Context.ACCESSIBILITY_SERVICE);

        List<AccessibilityServiceInfo> runningServices = am
                .getEnabledAccessibilityServiceList(AccessibilityEvent.TYPES_ALL_MASK);
        for (AccessibilityServiceInfo service : runningServices) {
            if (id.equals(service.getId())) {
                return true;
            }
        }

        return false;
    }

    public static boolean isNotificationAccessGiven(Context context) {
        boolean enabled = false;
        Set<String> enabledListenerPackagesSet = NotificationManagerCompat.getEnabledListenerPackages(context);
        for (String string: enabledListenerPackagesSet)
            if (string.contains(context.getPackageName())) enabled = true;
        return enabled;
    }
}
