package com.msa.tuananh.callflash.service.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsMessage;
import android.util.Log;

import com.msa.tuananh.callflash.utils.Constant;
import com.msa.tuananh.callflash.utils.FlashUtilities;
import com.msa.tuananh.callflash.utils.FlashlightProvider;
import com.msa.tuananh.callflash.utils.Utilities;

public class SmsBroadcastReceiver extends BroadcastReceiver {
    Runnable runnable;
    Runnable runnable1;
    Integer timesNhapNhay = 0;

    @Override
    public void onReceive(final Context context, Intent intent) {
        // TODO Auto-generated method stub

        final FlashlightProvider flashlightProvider = new FlashlightProvider(context);
        final Handler handler = new Handler();
        runnable1 = new Runnable() {
            @Override
            public void run() {
                timesNhapNhay += 1;
                Log.d("flash", "off: ");
                flashlightProvider.turnFlashlightOff();
                if (timesNhapNhay == Utilities.getSmsLight(context, Constant.SMS_TIME)) {
                    flashlightProvider.turnFlashlightOff();
                    handler.removeCallbacks(this);
                    handler.removeCallbacks(runnable);
                } else {
                    handler.postDelayed(runnable, Utilities.getSmsLight(context, Constant.SMS_OFF));
                }
            }
        };
        runnable = new Runnable() {
            @Override
            public void run() {
                flashlightProvider.turnFlashlightOn();
                Log.d("flash", "on: ");
                handler.postDelayed(runnable1, Utilities.getSmsLight(context, Constant.SMS_ON));
            }
        };

        //Order (Important first) : battery > service > disturb > sound > call

        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            Bundle bundle = intent.getExtras();
            final String type = FlashUtilities.getSoundProfile(context);
            final boolean isSound = Utilities.getSoundProfile(context, type);
            final int batteryCurrent = FlashUtilities.getBatteryPercentage(context);
            final int batteryThreshold = Utilities.getBatteryLevel(context);
            if (bundle != null) {
                if (batteryCurrent > batteryThreshold) {
                    if (Utilities.getSettings(context, "Service")) {
                        if (!FlashUtilities.dobHours(context)) {
                            if (isSound) {
                                if (Utilities.getSettings(context, "Sms")) {
                                    handler.postDelayed(runnable, Utilities.getSmsLight(context, Constant.SMS_OFF));
                                }
                            }
                        } else {
                            if (!FlashUtilities.dobHours(context)) {
                                if (isSound) {
                                    if (Utilities.getSettings(context, "Sms")) {
                                        if (!FlashUtilities.dobHours(context)) {
                                            handler.postDelayed(runnable, Utilities.getSmsLight(context, Constant.SMS_OFF));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

