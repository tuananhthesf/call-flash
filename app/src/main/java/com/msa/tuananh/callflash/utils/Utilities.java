package com.msa.tuananh.callflash.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.view.View;
import android.widget.Toast;

import java.util.Map;

public class Utilities {

    //SharedPreferences
    public static void saveSettings(Context context, Integer type, boolean service, boolean call, boolean sms) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        switch (type) {
            case 1:
                editor.putBoolean("Service", service);
                break;
            case 2:
                editor.putBoolean("Call", call);
                break;
            case 3:
                editor.putBoolean("Sms", sms);
                break;
            case 4:
                editor.putBoolean("Service", service);
                editor.putBoolean("Call", call);
                editor.putBoolean("Sms", sms);
                break;
        }
        editor.apply();
    }

    public static Boolean getSettings(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, false);
    }

    //---------------------------------------------------------
    public static void saveDisturbMode(Context context, int type, boolean disturb, String start, String end) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Disturb", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        switch (type) {
            case 1:
                editor.putBoolean("Disturb", disturb);
                break;
            case 2:
                editor.putString("Start", start);
                break;
            case 3:
                editor.putString("End", end);
                break;
            default:
                editor.putBoolean("Disturb", disturb);
                editor.putString("Start", start);
                editor.putString("End", end);
                break;
        }
        editor.apply();
    }

    public static boolean getDisturb(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Disturb", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("Disturb", false);
    }

    public static String getDisturbStart(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Disturb", Context.MODE_PRIVATE);
        return sharedPreferences.getString("Start", "00:00");
    }

    public static String getDisturbEnd(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Disturb", Context.MODE_PRIVATE);
        return sharedPreferences.getString("End", "00:00");
    }


    //---------------------------------------------------------

    public static void saveBatteryLevel(Context context, Integer batteryLvl) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Battery", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("Level", batteryLvl);
        editor.apply();
    }

    public static int getBatteryLevel(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Battery", Context.MODE_PRIVATE);
        return sharedPreferences.getInt("Level", 15);
    }

    //---------------------------------------------------------

    public static void saveSoundProfile(Context context, Integer type, boolean normal, boolean vibrate, boolean silent) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Sound", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        switch (type) {
            case 1:
                editor.putBoolean("Normal", normal);
                break;
            case 2:
                editor.putBoolean("Vibrate", vibrate);
                break;
            case 3:
                editor.putBoolean("Silent", silent);
                break;
            case 4:
                editor.putBoolean("Normal", normal);
                editor.putBoolean("Vibrate", vibrate);
                editor.putBoolean("Silent", silent);
                break;
        }
        editor.apply();
    }

    public static boolean getSoundProfile(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Sound", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, true);
    }

    //----------------------------------------------------------

    public static void saveNoti(Context context, boolean isOn) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Notification", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("Noti", isOn);
        editor.apply();
    }

    public static Boolean getNoti(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Notification", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("Noti", false);
    }

    public static void savePackage(Context context, String packageName, boolean isOn) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("NotiPackage", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(packageName, isOn);
        editor.apply();
    }

    public static Boolean getPackage(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("NotiPackage", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, false);
    }

    //----------------------------------------------------------

    public static void saveCallLight(Context context, String key, int ligth) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("CallLight", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, ligth);
        editor.apply();
    }

    public static int getCallLight(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("CallLight", Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, 500);
    }

    public static void saveSmsLight(Context context, String key, int ligth) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("SmsLight", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, ligth);
        editor.apply();
    }

    public static int getSmsLight(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("SmsLight", Context.MODE_PRIVATE);
        if (key.equalsIgnoreCase(Constant.SMS_TIME)) {
            return sharedPreferences.getInt(key, 5);
        } else {
            return sharedPreferences.getInt(key, 500);
        }
    }
}
