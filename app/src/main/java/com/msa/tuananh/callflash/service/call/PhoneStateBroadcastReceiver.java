package com.msa.tuananh.callflash.service.call;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.msa.tuananh.callflash.MainActivity;
import com.msa.tuananh.callflash.utils.Constant;
import com.msa.tuananh.callflash.utils.FlashUtilities;
import com.msa.tuananh.callflash.utils.FlashlightProvider;
import com.msa.tuananh.callflash.utils.Utilities;

/**
 * Created by kynt9 on 7/30/2018.
 **/

public class PhoneStateBroadcastReceiver extends BroadcastReceiver {
    Runnable runnable;
    Runnable runnable1;
    Integer timesNhapNhay = 0;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        try {
            TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            final FlashlightProvider flashlightProvider = new FlashlightProvider(context);
            final Handler handler = new Handler();
            runnable1 = new Runnable() {
                @Override
                public void run() {
                    timesNhapNhay += 1;
                    Log.d("flash", "off: ");
                    flashlightProvider.turnFlashlightOff();
                    handler.postDelayed(runnable, Utilities.getCallLight(context, Constant.CALL_OFF));
                }
            };
            runnable = new Runnable() {
                @Override
                public void run() {
                    flashlightProvider.turnFlashlightOn();
                    Log.d("flash", "on: ");
                    handler.postDelayed(runnable1, Utilities.getCallLight(context, Constant.CALL_ON));
                }
            };


            telephony.listen(new PhoneStateListener() {
                @Override
                public void onCallStateChanged(int state, final String incomingNumber) {
                    super.onCallStateChanged(state, incomingNumber);
                    switch (state) {

                        case TelephonyManager.CALL_STATE_IDLE:
                            Log.d("PPP", "Case = 0 không nghe máy");
                            try {
                                handler.removeCallbacks(runnable);
                                handler.removeCallbacks(runnable1);
                                flashlightProvider.turnFlashlightOff();
                            } catch (Exception e) {
                            }
                            break;
                        case TelephonyManager.CALL_STATE_RINGING:
                            Log.d("PPP", "onCallStateChanged: ");

                            final String type = FlashUtilities.getSoundProfile(context);
                            final boolean isSound = Utilities.getSoundProfile(context, type);
                            final int batteryCurrent = FlashUtilities.getBatteryPercentage(context);
                            final int batteryThreshold = Utilities.getBatteryLevel(context);

                            //Order (Important first) : battery > service > disturb > sound > call

                            if (batteryCurrent > batteryThreshold) {
                                if (Utilities.getSettings(context, "Service")) {
                                    if (!Utilities.getDisturb(context)) {
                                        if (isSound) {
                                            if (Utilities.getSettings(context, "Call")) {
                                                try {
                                                    handler.postDelayed(runnable, Utilities.getCallLight(context, Constant.CALL_OFF));
                                                } catch (Exception e) {
                                                }
                                            }
                                        }
                                    } else {
                                        if (!FlashUtilities.dobHours(context)) {
                                            if (isSound) {
                                                if (Utilities.getSettings(context, "Call")) {
                                                    try {
                                                        handler.postDelayed(runnable, Utilities.getCallLight(context, Constant.CALL_OFF));
                                                    } catch (Exception e) {
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;

                        case TelephonyManager.CALL_STATE_OFFHOOK:
                            Log.d("PPP", "Case = 2 nghe máy");

                            try {
                                handler.removeCallbacks(runnable);
                                handler.removeCallbacks(runnable1);
                                flashlightProvider.turnFlashlightOff();
                            } catch (Exception e) {
                            }

                            break;
                    }
                }
            }, PhoneStateListener.LISTEN_CALL_STATE);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
